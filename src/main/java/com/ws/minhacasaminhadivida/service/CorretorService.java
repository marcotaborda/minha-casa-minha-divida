package com.ws.minhacasaminhadivida.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ws.minhacasaminhadivida.domain.Corretor;
import com.ws.minhacasaminhadivida.repository.CorretorRepository;

@Service
public class CorretorService {

	private final CorretorRepository corretorRepository;
	
	@Autowired
	public CorretorService(CorretorRepository corretorRepository) {
		this.corretorRepository = corretorRepository;
	}
	
	public Iterable<Corretor> lista() {
		return corretorRepository.findAll();
	}
	
	public Corretor salvar(Corretor corretor) {
		return corretorRepository.save(corretor);
	}

	public Boolean excluir(Corretor corretor) {
		try {
			corretorRepository.delete(corretor);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
