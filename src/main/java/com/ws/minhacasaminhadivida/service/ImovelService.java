package com.ws.minhacasaminhadivida.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ws.minhacasaminhadivida.domain.Imovel;
import com.ws.minhacasaminhadivida.repository.ImovelRepository;

@Service
public class ImovelService {

	private final ImovelRepository imovelRepository;
	
	@Autowired
	public ImovelService(ImovelRepository imovelRepository) {
		this.imovelRepository = imovelRepository;
	}
	
	public Iterable<Imovel> listaComParametros(String endereco, Boolean alarmeAtivo) {
    return imovelRepository.findByParametros(endereco, alarmeAtivo);
  }
	
	public Iterable<Imovel> lista() {
		return imovelRepository.findAll();
	}
	
	public Imovel salvar(Imovel imovel) {
		return imovelRepository.save(imovel);
	}

	public Boolean excluir(Imovel imovel) {
		try {
			imovelRepository.delete(imovel);	
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
