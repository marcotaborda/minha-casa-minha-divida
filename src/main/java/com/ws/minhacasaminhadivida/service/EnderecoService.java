package com.ws.minhacasaminhadivida.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ws.minhacasaminhadivida.domain.Endereco;
import com.ws.minhacasaminhadivida.repository.EnderecoRepository;

@Service
public class EnderecoService {

	private final EnderecoRepository enderecoRepository;
	
	@Autowired
	public EnderecoService(EnderecoRepository enderecoRepository) {
		this.enderecoRepository = enderecoRepository;
	}
	
	public Iterable<Endereco> lista() {
		return enderecoRepository.findAll();
	}
	
	public Endereco salvar(Endereco endereco) {
		return enderecoRepository.save(endereco);
	}

	public Boolean excluir(Endereco endereco) {
		try {
			enderecoRepository.delete(endereco);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
