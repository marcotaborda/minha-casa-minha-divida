package com.ws.minhacasaminhadivida.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ws.minhacasaminhadivida.domain.Proprietario;
import com.ws.minhacasaminhadivida.repository.ProprietarioRepository;

@Service
public class ProprietarioService {

	private final ProprietarioRepository proprietarioRepository;
	
	@Autowired
	public ProprietarioService(ProprietarioRepository proprietarioRepository) {
		this.proprietarioRepository = proprietarioRepository;
	}
	
	public Iterable<Proprietario> lista() {
		return proprietarioRepository.findAll();
	}
	
	public Proprietario salvar(Proprietario proprietario) {
		return proprietarioRepository.save(proprietario);
	}

	public Boolean excluir(Proprietario proprietario) {
		try {
			proprietarioRepository.delete(proprietario);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
