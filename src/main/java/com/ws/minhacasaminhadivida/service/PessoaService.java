package com.ws.minhacasaminhadivida.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ws.minhacasaminhadivida.domain.Pessoa;
import com.ws.minhacasaminhadivida.repository.PessoaRepository;

@Service
public class PessoaService {

	private final PessoaRepository pessoaRepository;
	
	@Autowired
	public PessoaService(PessoaRepository pessoaRepository) {
		this.pessoaRepository = pessoaRepository;
	}
	
	public Iterable<Pessoa> lista() {
		return pessoaRepository.findAll();
	}
	
	public Pessoa salvar(Pessoa pessoa) {
		return pessoaRepository.save(pessoa);
	}

	public Boolean excluir(Pessoa pessoa) {
		try {
			pessoaRepository.delete(pessoa);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
