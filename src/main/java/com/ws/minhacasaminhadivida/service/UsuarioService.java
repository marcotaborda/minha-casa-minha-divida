package com.ws.minhacasaminhadivida.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ws.minhacasaminhadivida.domain.Usuario;
import com.ws.minhacasaminhadivida.exception.LoginException;
import com.ws.minhacasaminhadivida.repository.UsuarioRepository;

import net.bytebuddy.utility.RandomString;

@Service
public class UsuarioService {

	private final UsuarioRepository usuarioRepository;
	
	@Autowired
	public UsuarioService(UsuarioRepository usuarioRepository) {
		this.usuarioRepository = usuarioRepository;
	}
	
	public Iterable<Usuario> lista() {
		return usuarioRepository.findAll();
	}
	
	public Usuario salvar(Usuario usuario) throws LoginException {
	  BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	  usuario.setSenha(passwordEncoder.encode(usuario.getSenha()));
	  
	  Usuario usuarioLogin = usuarioRepository.findByLogin(usuario.getUsername());
	  
	  if (Optional.ofNullable(usuarioLogin).isPresent()) {
	    throw new LoginException("Nome de usuário já cadastrado.");
	  }
		return usuarioRepository.save(usuario);
	}

	public Boolean excluir(Usuario usuario) {
		try {
			usuarioRepository.delete(usuario);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public Usuario logar(String username, String senha) throws LoginException {
	  Usuario usuarioLogin = usuarioRepository.findByLogin(username);
	  
	  BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    Boolean isSenhaCorreta = passwordEncoder.matches(senha, usuarioLogin.getSenha());
	  
		if (!isSenhaCorreta) {
			throw new LoginException("A senha informada está incorreta.");
		}
		criarToken(usuarioLogin);
		
		return usuarioLogin;
	}
	
	public void criarToken(Usuario usuario) {
		usuario.setToken_acesso(RandomString.hashOf(255));
		usuarioRepository.save(usuario);
	}
	
	public Boolean removerToken(Usuario usuario) {
	  Usuario usuarioLogin = usuarioRepository.findByLogin(usuario.getUsername());
		try {
		  usuarioLogin.setToken_acesso(null);
			usuarioRepository.save(usuarioLogin);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
