package com.ws.minhacasaminhadivida.exception;

public class LoginException extends Exception {

  public LoginException(String errorMessage) {
      super(errorMessage);
  }
}
