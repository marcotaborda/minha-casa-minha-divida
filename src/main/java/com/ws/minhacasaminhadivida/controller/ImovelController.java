package com.ws.minhacasaminhadivida.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ws.minhacasaminhadivida.domain.Imovel;
import com.ws.minhacasaminhadivida.dto.ImovelDTO;
import com.ws.minhacasaminhadivida.service.ImovelService;

@RestController
@RequestMapping("/imoveis")
public class ImovelController {

	private final ImovelService imovelService;
	
	private static final String ALARME_DEFAULT = "0";
	
	@Autowired
	public ImovelController(ImovelService imovelService) {
		this.imovelService = imovelService;
	}
	
	@GetMapping("/listar")
	public ResponseEntity<Iterable<Imovel>> listar(@RequestParam Optional<String> endereco, @RequestParam(defaultValue = ALARME_DEFAULT) String alarme) {
	  if (endereco.isPresent()) {
	    return new ResponseEntity<>(imovelService.listaComParametros(endereco.get(), !alarme.equals(ALARME_DEFAULT)), HttpStatus.OK);	    
	  }
		return new ResponseEntity<>(imovelService.lista(), HttpStatus.OK);
	}
	
	@PostMapping("/cadastrar")
	public ResponseEntity<Imovel> salvar(@RequestBody ImovelDTO imovelDTO) {
		Imovel imovel = imovelService.salvar(imovelDTO.transformaParaObjeto());
		return new ResponseEntity<>(imovel, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/excluir")
	public ResponseEntity<Boolean> excluir(@RequestBody ImovelDTO imovelDTO) {
		Boolean isExcluido = imovelService.excluir(imovelDTO.transformaParaObjeto());
		return new ResponseEntity<>(isExcluido, HttpStatus.NO_CONTENT);
	}
}
