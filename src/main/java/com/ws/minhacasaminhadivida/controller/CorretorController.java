package com.ws.minhacasaminhadivida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ws.minhacasaminhadivida.domain.Corretor;
import com.ws.minhacasaminhadivida.dto.CorretorDTO;
import com.ws.minhacasaminhadivida.service.CorretorService;

@RestController
@RequestMapping("/corretores")
public class CorretorController {

	private final CorretorService corretorService;
	
	@Autowired
	public CorretorController(CorretorService corretorService) {
		this.corretorService = corretorService;
	}
	
	@GetMapping("/listar")
	public ResponseEntity<Iterable<Corretor>> listar() {
		return new ResponseEntity<>(corretorService.lista(), HttpStatus.OK);
	}
	
	@PostMapping("/cadastrar")
	public ResponseEntity<Corretor> salvar(@RequestBody CorretorDTO corretorDTO) {
		Corretor corretor = corretorService.salvar(corretorDTO.transformaParaObjeto());
		return new ResponseEntity<>(corretor, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/excluir")
	public ResponseEntity<Boolean> excluir(@RequestBody CorretorDTO corretorDTO) {
		Boolean isExcluido = corretorService.excluir(corretorDTO.transformaParaObjeto());
		return new ResponseEntity<>(isExcluido, HttpStatus.NO_CONTENT);
	}
}
