package com.ws.minhacasaminhadivida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ws.minhacasaminhadivida.domain.Usuario;
import com.ws.minhacasaminhadivida.dto.UsuarioDTO;
import com.ws.minhacasaminhadivida.exception.LoginException;
import com.ws.minhacasaminhadivida.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	private final UsuarioService usuarioService;
	
	@Autowired
	public UsuarioController(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@GetMapping("/listar")
	public ResponseEntity<Iterable<Usuario>> listar() {
		return new ResponseEntity<>(usuarioService.lista(), HttpStatus.OK);
	}
	
	@PostMapping("/cadastrar")
	public ResponseEntity<Usuario> salvar(@RequestBody UsuarioDTO usuarioDTO) throws LoginException {
		Usuario usuario = usuarioService.salvar(usuarioDTO.transformaParaObjeto());
		return new ResponseEntity<>(usuario, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/excluir")
	public ResponseEntity<Boolean> excluir(@RequestBody UsuarioDTO usuarioDTO) {
		Boolean isExcluido = usuarioService.excluir(usuarioDTO.transformaParaObjeto());
		return new ResponseEntity<>(isExcluido, HttpStatus.NO_CONTENT);
	}
}
