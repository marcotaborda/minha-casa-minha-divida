package com.ws.minhacasaminhadivida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ws.minhacasaminhadivida.domain.Proprietario;
import com.ws.minhacasaminhadivida.dto.ProprietarioDTO;
import com.ws.minhacasaminhadivida.service.ProprietarioService;

@RestController
@RequestMapping("/proprietarios")
public class ProprietarioController {

	private final ProprietarioService proprietarioService;
	
	@Autowired
	public ProprietarioController(ProprietarioService proprietarioService) {
		this.proprietarioService = proprietarioService;
	}
	
	@GetMapping("/listar")
	public ResponseEntity<Iterable<Proprietario>> listar() {
		return new ResponseEntity<>(proprietarioService.lista(), HttpStatus.OK);
	}
	
	@PostMapping("/cadastrar")
	public ResponseEntity<Proprietario> salvar(@RequestBody ProprietarioDTO proprietarioDTO) {
		Proprietario proprietario = proprietarioService.salvar(proprietarioDTO.transformaParaObjeto());
		return new ResponseEntity<>(proprietario, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/excluir")
	public ResponseEntity<Boolean> excluir(@RequestBody ProprietarioDTO proprietarioDTO) {
		Boolean isExcluido = proprietarioService.excluir(proprietarioDTO.transformaParaObjeto());
		return new ResponseEntity<>(isExcluido, HttpStatus.NO_CONTENT);
	}
}
