package com.ws.minhacasaminhadivida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ws.minhacasaminhadivida.domain.Pessoa;
import com.ws.minhacasaminhadivida.dto.PessoaDTO;
import com.ws.minhacasaminhadivida.service.PessoaService;

@RestController
@RequestMapping("/pessoas")
public class PessoaController {

	private final PessoaService pessoaService;
	
	@Autowired
	public PessoaController(PessoaService pessoaService) {
		this.pessoaService = pessoaService;
	}
	
	@GetMapping("/listar")
	public ResponseEntity<Iterable<Pessoa>> listar() {
		return new ResponseEntity<>(pessoaService.lista(), HttpStatus.OK);
	}
	
	@PostMapping("/cadastrar")
	public ResponseEntity<Pessoa> salvar(@RequestBody PessoaDTO pessoaDTO) {
		Pessoa pessoa = pessoaService.salvar(pessoaDTO.transformaParaObjeto());
		return new ResponseEntity<>(pessoa, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/excluir")
	public ResponseEntity<Boolean> excluir(@RequestBody PessoaDTO pessoaDTO) {
		Boolean isExcluido = pessoaService.excluir(pessoaDTO.transformaParaObjeto());
		return new ResponseEntity<>(isExcluido, HttpStatus.NO_CONTENT);
	}
}
