package com.ws.minhacasaminhadivida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ws.minhacasaminhadivida.domain.Usuario;
import com.ws.minhacasaminhadivida.dto.UsuarioDTO;
import com.ws.minhacasaminhadivida.service.UsuarioService;

@RestController
@RequestMapping("/auth")
public class AutenticacaoController {

private final UsuarioService usuarioService;
	
	@Autowired
	public AutenticacaoController(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@PostMapping("/login")
	public ResponseEntity<Usuario> login(@RequestBody UsuarioDTO usuarioDTO) throws Exception {
	  Usuario usuario = usuarioDTO.transformaParaObjeto();
		Usuario usuarioLogin = usuarioService.logar(usuario.getUsername(), usuario.getSenha());
		return new ResponseEntity<>(usuarioLogin, HttpStatus.OK);
	}
	
	@PostMapping("/logout")
	public ResponseEntity<Boolean> logout(@RequestBody UsuarioDTO usuario) {
		Boolean isDeslogado = usuarioService.removerToken(usuario.transformaParaObjeto());
		return new ResponseEntity<>(isDeslogado, HttpStatus.NO_CONTENT);
	}
}
