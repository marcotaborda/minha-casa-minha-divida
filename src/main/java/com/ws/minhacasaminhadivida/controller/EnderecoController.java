package com.ws.minhacasaminhadivida.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ws.minhacasaminhadivida.domain.Endereco;
import com.ws.minhacasaminhadivida.dto.EnderecoDTO;
import com.ws.minhacasaminhadivida.service.EnderecoService;

@RestController
@RequestMapping("/enderecos")
public class EnderecoController {

	private final EnderecoService enderecoService;
	
	@Autowired
	public EnderecoController(EnderecoService enderecoService) {
		this.enderecoService = enderecoService;
	}
	
	@GetMapping("/listar")
	public ResponseEntity<Iterable<Endereco>> listar() {
		return new ResponseEntity<Iterable<Endereco>>(enderecoService.lista(), HttpStatus.OK);
	}
	
	@PostMapping("/cadastrar")
	public ResponseEntity<Endereco> salvar(@RequestBody EnderecoDTO enderecoDTO) {
		Endereco endereco = enderecoService.salvar(enderecoDTO.transformaParaObjeto());
		return new ResponseEntity<>(endereco, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/excluir")
	public ResponseEntity<Boolean> excluir(@RequestBody EnderecoDTO enderecoDTO) {
		Boolean isExcluido = enderecoService.excluir(enderecoDTO.transformaParaObjeto());
		return new ResponseEntity<>(isExcluido, HttpStatus.NO_CONTENT);
	}
}
