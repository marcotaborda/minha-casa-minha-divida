package com.ws.minhacasaminhadivida.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ws.minhacasaminhadivida.domain.Corretor;

@Repository
public interface CorretorRepository extends CrudRepository<Corretor, Long> {
}
