package com.ws.minhacasaminhadivida.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ws.minhacasaminhadivida.domain.Endereco;

@Repository
public interface EnderecoRepository extends CrudRepository<Endereco, Long> {
}
