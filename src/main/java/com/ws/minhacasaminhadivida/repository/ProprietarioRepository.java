package com.ws.minhacasaminhadivida.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ws.minhacasaminhadivida.domain.Proprietario;

@Repository
public interface ProprietarioRepository extends CrudRepository<Proprietario, Long> {
}
