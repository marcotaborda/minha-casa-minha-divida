package com.ws.minhacasaminhadivida.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ws.minhacasaminhadivida.domain.Imovel;

@Repository
public interface ImovelRepository extends CrudRepository<Imovel, Long> {

  @Query("SELECT i FROM Imovel i INNER JOIN i.endereco e WHERE e.endereco LIKE %:endereco% AND i.alarme_ultima_atualizacao = :alarme")
  public Iterable<Imovel> findByParametros(String endereco, Boolean alarme);
}
