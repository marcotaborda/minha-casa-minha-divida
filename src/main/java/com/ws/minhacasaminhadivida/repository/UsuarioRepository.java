package com.ws.minhacasaminhadivida.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ws.minhacasaminhadivida.domain.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
	
	@Query(value = "SELECT u FROM Usuario u WHERE username = :username")
	Usuario findByLogin(String username);
}
