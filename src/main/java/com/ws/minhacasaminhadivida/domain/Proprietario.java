package com.ws.minhacasaminhadivida.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "proprietarios")
public class Proprietario {

	@Id
	@Column(name = "id_proprietario")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_proprietario;
	
	@OneToOne
	@JoinColumn(name = "id_pessoa")
	private Pessoa pessoa;
	
	public Proprietario() {
		super();
	}

	public Proprietario(Integer id_proprietario, Pessoa pessoa) {
		this.id_proprietario = id_proprietario;
		this.pessoa = pessoa;
	}

	public Integer getId_proprietario() {
		return id_proprietario;
	}

	public void setId_proprietario(Integer id_proprietario) {
		this.id_proprietario = id_proprietario;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
}
