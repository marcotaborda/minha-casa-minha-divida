package com.ws.minhacasaminhadivida.domain;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "imoveis")
public class Imovel {

	@Id
	@Column(name = "id_imovel")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_imovel;
	
	@OneToOne
	@JoinColumn(name = "id_endereco")
	private Endereco endereco;
	
	@OneToOne
	@JoinColumn(name = "id_corretor")
	private Corretor corretor;
	
	@OneToOne
	@JoinColumn(name = "id_proprietario")
	private Proprietario proprietario;
	
	@Column(name = "financiado")
	private Boolean financiado;
	
	@Column(name = "pertence_proprietario")
	private Boolean pertence_proprietario;
	
	@Column(name = "aceita_negociacao")
	private Boolean aceita_negociacao;
	
	@Column(name = "valor")
	private Float valor;
	
	@Column(name = "imagem")
	private String imagem;
	
	@Column(name = "data_cadastro")
	private Date data_cadastro;
	
	@Column(name = "data_atualizacao")
	private Date data_atualizacao;
	
	@Column(name = "alarme_ultima_atualizacao")
	private Boolean alarme_ultima_atualizacao;
	
	@OneToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;
	
	public Imovel() {
		super();
	}

	public Imovel(Integer id_imovel, Endereco endereco, Corretor corretor, Proprietario proprietario, Boolean financiado,
			Boolean pertence_proprietario, Boolean aceita_negociacao, Float valor, String imagem, Date data_cadastro,
			Date data_atualizacao, Boolean alarme_ultima_atualizacao, Usuario usuario) {
		this.id_imovel = id_imovel;
		this.endereco = endereco;
		this.corretor = corretor;
		this.proprietario = proprietario;
		this.financiado = financiado;
		this.pertence_proprietario = pertence_proprietario;
		this.aceita_negociacao = aceita_negociacao;
		this.valor = valor;
		this.imagem = imagem;
		this.data_cadastro = data_cadastro;
		this.data_atualizacao = data_atualizacao;
		this.alarme_ultima_atualizacao = alarme_ultima_atualizacao;
		this.usuario = usuario;
	}

	public Integer getId_imovel() {
		return id_imovel;
	}

	public void setId_imovel(Integer id_imovel) {
		this.id_imovel = id_imovel;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public Corretor getCorretor() {
		return corretor;
	}

	public void setCorretor(Corretor corretor) {
		this.corretor = corretor;
	}

	public Proprietario getProprietario() {
		return proprietario;
	}

	public void setProprietario(Proprietario proprietario) {
		this.proprietario = proprietario;
	}

	public Boolean getFinanciado() {
		return financiado;
	}

	public void setFinanciado(Boolean financiado) {
		this.financiado = financiado;
	}

	public Boolean getPertence_proprietario() {
		return pertence_proprietario;
	}

	public void setPertence_proprietario(Boolean pertence_proprietario) {
		this.pertence_proprietario = pertence_proprietario;
	}

	public Boolean getAceita_negociacao() {
		return aceita_negociacao;
	}

	public void setAceita_negociacao(Boolean aceita_negociacao) {
		this.aceita_negociacao = aceita_negociacao;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public Date getData_cadastro() {
		return data_cadastro;
	}

	public void setData_cadastro(Date data_cadastro) {
		this.data_cadastro = data_cadastro;
	}

	public Date getData_atualizacao() {
		return data_atualizacao;
	}

	public void setData_atualizacao(Date data_atualizacao) {
		this.data_atualizacao = data_atualizacao;
	}

	public Boolean getAlarme_ultima_atualizacao() {
		return alarme_ultima_atualizacao;
	}

	public void setAlarme_ultima_atualizacao(Boolean alarme_ultima_atualizacao) {
		this.alarme_ultima_atualizacao = alarme_ultima_atualizacao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
