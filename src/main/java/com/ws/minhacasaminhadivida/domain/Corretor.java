package com.ws.minhacasaminhadivida.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "corretores")
public class Corretor {

	@Id
	@Column(name = "id_corretor")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id_corretor;
	
	@OneToOne
	@JoinColumn(name = "id_pessoa")
	private Pessoa pessoa;
	
	public Corretor() {
		super();
	}

	public Corretor(Integer id_corretor, Pessoa pessoa) {
		this.id_corretor = id_corretor;
		this.pessoa = pessoa;
	}

	public Integer getId_corretor() {
		return id_corretor;
	}

	public void setId_corretor(Integer id_corretor) {
		this.id_corretor = id_corretor;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
}
