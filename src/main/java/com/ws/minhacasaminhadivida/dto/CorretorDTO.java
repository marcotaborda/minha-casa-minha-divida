package com.ws.minhacasaminhadivida.dto;

import com.ws.minhacasaminhadivida.domain.Corretor;
import com.ws.minhacasaminhadivida.domain.Pessoa;

public class CorretorDTO {

	public Integer id_corretor;
	public Pessoa pessoa;

	public Corretor transformaParaObjeto() {
		return new Corretor(id_corretor, pessoa);
	}
}
