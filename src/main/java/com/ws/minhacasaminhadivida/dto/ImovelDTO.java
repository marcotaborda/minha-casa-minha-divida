package com.ws.minhacasaminhadivida.dto;

import java.sql.Date;

import com.ws.minhacasaminhadivida.domain.Corretor;
import com.ws.minhacasaminhadivida.domain.Endereco;
import com.ws.minhacasaminhadivida.domain.Imovel;
import com.ws.minhacasaminhadivida.domain.Proprietario;
import com.ws.minhacasaminhadivida.domain.Usuario;

public class ImovelDTO {

	public Integer id_imovel;
	public Endereco endereco;
	public Corretor corretor;
	public Proprietario proprietario;
	public Boolean financiado;
	public Boolean pertence_proprietario;
	public Boolean aceita_negociacao;
	public String imagem;
	public Float valor;
	public Date data_cadastro;
	public Date data_atualizacao;
	public Boolean alarme_ultima_atualizacao;
	public Usuario usuario;
	
	public Imovel transformaParaObjeto() {
		return new Imovel(id_imovel, endereco, corretor, proprietario, financiado, pertence_proprietario, aceita_negociacao, valor, imagem, data_cadastro, data_atualizacao, alarme_ultima_atualizacao, usuario);
	}
}
