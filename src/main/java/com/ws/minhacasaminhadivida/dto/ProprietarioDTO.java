package com.ws.minhacasaminhadivida.dto;

import com.ws.minhacasaminhadivida.domain.Pessoa;
import com.ws.minhacasaminhadivida.domain.Proprietario;

public class ProprietarioDTO {

	public Integer id_proprietario;
	public Pessoa pessoa;

	public Proprietario transformaParaObjeto() {
		return new Proprietario(id_proprietario, pessoa);
	}
}
