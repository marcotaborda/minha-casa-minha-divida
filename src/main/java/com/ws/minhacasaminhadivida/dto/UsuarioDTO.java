package com.ws.minhacasaminhadivida.dto;

import java.sql.Date;

import com.ws.minhacasaminhadivida.domain.Pessoa;
import com.ws.minhacasaminhadivida.domain.Usuario;

public class UsuarioDTO {

	public Integer id_usuario;
	public Pessoa pessoa;
	public String username;
	public String senha;
	public Date data_acesso;

	public Usuario transformaParaObjeto() {
		return new Usuario(id_usuario, pessoa, username, senha, data_acesso);
	}
}
