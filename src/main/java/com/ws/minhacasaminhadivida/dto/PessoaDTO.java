package com.ws.minhacasaminhadivida.dto;

import com.ws.minhacasaminhadivida.domain.Endereco;
import com.ws.minhacasaminhadivida.domain.Pessoa;

public class PessoaDTO {

	public Integer id_pessoa;
	public Endereco endereco;
	public String nome;
	public String email;
	public String telefone;

	public Pessoa transformaParaObjeto() {
		return new Pessoa(id_pessoa, endereco, nome, email, telefone);
	}
}
