package com.ws.minhacasaminhadivida.dto;

import com.ws.minhacasaminhadivida.domain.Endereco;

public class EnderecoDTO {

	public Integer id_endereco;
	public String endereco;
	public String numero;
	public String bairro;
	public String cidade;
	public String pais;

	public Endereco transformaParaObjeto() {
		return new Endereco(id_endereco, endereco, numero, bairro, cidade, pais);
	}
}
